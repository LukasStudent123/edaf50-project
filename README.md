# EDAF50-Project
Project in the course EDAF50 - C++ programming <br />
Featuring a newsserver with a client and two servers, <br />
one in memory and one using a database on disk.<br />

## Compiling the server
In the root directory, run make:<br />
```console
edaf50-project$ make
```
This compiles the source code in `src`.

## Installing and running
### Install
Install the server using make:
```console
edaf50-project$ make install
```
This creates a new directory `bin` in the root directory, <br />
and copies the binaries `server_disk`, `server_memory` and `myclient` <br />
to the `bin` directory 

### Running
Navigate to the `bin` directory:
```console
edaf50-project$ cd bin
```
To use the in-memory server, run `server_memory` and specify a port:
```console
bin$ ./server_memory [port-nr]
```
To use the database server, run `server_disk`:
```console
bin$ ./server_disk [port-nr]
```
The database is stored as a file tree under `database_dir` in `bin`

In another terminal, navigate to the `bin` directory and run the client:
```console
bin$ ./myclient localhost [port-nr]
```

## Cleaning or uninstalling
To remove object files, run clean from the root directory:
```console
edaf50-project$ make clean
```
To remove dependency files as well, run distclean:
```console
edaf50-project$ make distclean
```

The whole server can be uninstalled by running uninstall:
```console
edaf50-project$ make uninstall
```
This removes all binaries, object files and dependency files, <br />
as well as any eventual database data.