
#include <array>
#include <memory>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>


#include "database_interface.h"
#include "database_disk.h"
#include "connectionclosedexception.h"

// Execute a shell command and get string output
std::string DatabaseDisk::execute_shell(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw ConnectionClosedException();
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

DatabaseDisk::DatabaseDisk() 
{ 
  // Gives 1 if false
  if(system("test -d database_dir"))
  {
    system("mkdir database_dir");
  }
  syncMap();
}

DatabaseDisk::~DatabaseDisk() {} 

void DatabaseDisk::syncMap()
{
  news_id = 1;

  std::string shell_res = execute_shell("cd database_dir && ls");
  if(shell_res == "")
  {
    return;
  }
  shell_res = execute_shell("cd database_dir && ls -d */");
  
  std::stringstream ng_string_stream(shell_res);
  std::string split_ng;

  while(std::getline(ng_string_stream, split_ng, '\n'))
  {
    int art_id = 0;

    int ng_id = std::stoi(split_ng);

    std::string cmd_get_ng_title = "cat database_dir/" + split_ng + "/title.newsgroup";
    std::string ng_title = execute_shell(cmd_get_ng_title.c_str());

    std::string ng_dir_cmd = "cd database_dir/" + split_ng + " && ls";
    if(newsgroups.count(ng_id) == 0)
    {
      Newsgroup ng(ng_id, ng_title);
      newsgroups.emplace(ng_id, ng);
    }
    Newsgroup& curr_ng = newsgroups.at(ng_id);
    auto& ng_articles = curr_ng.getArticles();

    news_id = std::max(static_cast<int>(news_id), ng_id);

    std::string article_folders = execute_shell(ng_dir_cmd.c_str());
    std::stringstream article_folder_string_stream(article_folders);
    std::string article_folder_name;

    while(std::getline(article_folder_string_stream, article_folder_name,'\n'))
    {
      if(article_folder_name != "title.newsgroup")
      {
        int article_id = std::stoi(article_folder_name);

        art_id = std::max(art_id, article_id);

        if(ng_articles.count(article_id) == 0)
        {
          std::string article_dir_path = "database_dir/" + split_ng + article_folder_name + "/";

          std::string cat_article_title = "cat " + article_dir_path + "title.txt";
          std::string cat_article_author = "cat " + article_dir_path + "author.txt"; 
          std::string cat_article_body = "cat " + article_dir_path + "body.txt";

          std::string title = execute_shell(cat_article_title.c_str());
          std::string author = execute_shell(cat_article_author.c_str());
          std::string body = execute_shell(cat_article_body.c_str());

          Article art(article_id, title, author, body);
          ng_articles.emplace(article_id, art);
       }
      }
    }
    curr_ng.setArtId(art_id + 1);
  }
  news_id++;
}

const std::vector<Newsgroup> DatabaseDisk::getAllNewsgroups()
{
  std::vector<Newsgroup> ng_vec;
  for(auto& it : newsgroups)
  {
    ng_vec.emplace_back(it.second);
  }
  return ng_vec;
}

// Store newsgroup info in file info.newsgroup
bool DatabaseDisk::addNewsgroup(std::string newsgroup_title)
{
  for(auto& it : newsgroups)
  {
    if(it.second.getNewsgroupTitle() == newsgroup_title)
    {
      return false;
    }
  }

  std::string cmd_mkdir = "mkdir database_dir/" + std::to_string(news_id);
  std::string cmd_file = "echo \"" + newsgroup_title + "\" >> database_dir/" + std::to_string(news_id) +"/title.newsgroup";

  execute_shell(cmd_mkdir.c_str());
  execute_shell(cmd_file.c_str());

  Newsgroup ng(news_id, newsgroup_title);
  newsgroups.emplace(news_id,ng);
  news_id++;
  return true;
}

bool DatabaseDisk::deleteNewsgroup(ID_Number id)
{ 
  if(newsgroups.count(id) == 0)
  {
    return false;
  }
  
  std::string ng_title = newsgroups.at(id).getNewsgroupTitle();

  std::string cmd = "rm -r database_dir/" +std::to_string(id);
  execute_shell(cmd.c_str());
  newsgroups.erase(id);
  return true;
}

const std::pair<int,std::vector<Article>> DatabaseDisk::getAllArticles(ID_Number newsgroup_id)
{
  std::vector<Article> art_vec;
  if(newsgroups.count(newsgroup_id) == 0)
  {
    return {-1,art_vec};
  }
  auto& ng = newsgroups.at(newsgroup_id);
  for(const auto& it : ng.getArticles())
  {
    art_vec.push_back(it.second);
  }
  return {0,art_vec};
}

// Create one file per string ffs
bool DatabaseDisk::addArticle(ID_Number newsgroup_id, std::string article_title, std::string author, std::string content)
{
  if(newsgroups.count(newsgroup_id) == 0)
  {
    return false;
  }
  Newsgroup& ng = newsgroups.at(newsgroup_id);
  ID_Number art_id = ng.createArticle(article_title, author, content);

  std::string art_dir = "database_dir/"+ std::to_string(ng.getNewsgroupId()) + "/" + std::to_string(art_id);

  std::string cmd_create_dir = "mkdir " + art_dir;

  std::string title_file = art_dir + "/title.txt";
  std::string author_file = art_dir + "/author.txt";
  std::string body_file = art_dir + "/body.txt";

  std::string create_title = "echo \"" + article_title +"\" >> " + title_file;
  std::string create_author = "echo \"" + author +"\" >> " + author_file;
  std::string create_body = "echo \"" + content + "\" >> " + body_file; 

  execute_shell(cmd_create_dir.c_str());
  execute_shell(create_title.c_str());
  execute_shell(create_author.c_str());
  execute_shell(create_body.c_str());

  return true;
}

int DatabaseDisk::deleteArticle(ID_Number newsgroup_id, ID_Number article_id)
{
  if(newsgroups.count(newsgroup_id) == 0)
  {
    return -1;
  }
  Newsgroup& ng = newsgroups.at(newsgroup_id);

  if(ng.getArticles().count(article_id) == 0)
  {
    return 1;
  }

  std::string article_path = "database_dir/" + std::to_string(ng.getNewsgroupId()) + "/" + std::to_string(article_id);
  std::string cmd = "rm -r " + article_path;

  execute_shell(cmd.c_str());

  ng.getArticles().erase(article_id);
  
  return 0;
}

std::pair<int,Article> DatabaseDisk::getArticle(ID_Number newsgroup_id, ID_Number article_id)
{

  if(newsgroups.count(newsgroup_id) == 0)
  {
    Article f(-1,"","","");
    return {-1, f};   
  }

  if(newsgroups.at(newsgroup_id).getArticles().count(article_id) == 0)
  {
    Article f(-1,"","","");
    return {1, f};   
  }

  Article a = newsgroups.at(newsgroup_id).getArticles().at(article_id);
  return {0, a};
} 

