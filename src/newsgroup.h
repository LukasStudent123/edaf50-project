#ifndef NEWSGROUP_H
#define NEWSGROUP_H

#include "article.h"
#include <map>
#include <string>

using ID_Number = std::size_t;

class Newsgroup {
public:
	Newsgroup(ID_Number id, std::string title) : newsgroup_id(id), newsgroup_title(title) {};

	ID_Number getNewsgroupId() {return newsgroup_id;};
	std::string getNewsgroupTitle() {return newsgroup_title;};

	std::map<ID_Number, Article>& getArticles() {return articles;}; //Unsure about the return type
	ID_Number createArticle(std::string title, std::string author, std::string text);
	ID_Number createArticle(std::string title, std::string author, std::string text, ID_Number art_id);

	bool deleteArticle(ID_Number id);
	std::pair<int,Article> getArticle(ID_Number id);

	void setArtId(ID_Number id){art_id = id;};
private:
	std::map<ID_Number, Article> articles;
	ID_Number art_id{1}; //Increase by 1 each time an article is created.

	// Private newsgroup members
	ID_Number newsgroup_id;
	std::string newsgroup_title;
};

#endif
