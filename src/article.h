#ifndef ARTICLE_H
#define ARTICLE_H

#include <string>

using std::string;
using ID_Number = std::size_t;

class Article {
public:
	Article(ID_Number id, string title, string author, string text) : id_nr{id}, title{title}, author{author}, text{text} {};
	const string& getTitle() const {return title;}
	const string& getAuthor() const {return author;}
	const string& getText() const {return text;}
	ID_Number getId() {return id_nr;};
private:
	const ID_Number id_nr;
	string title;
	string author;
	string text;
};

#endif
