/* myclient.cc: sample client program */
#include "connection.h"
#include "connectionclosedexception.h"
#include "messagehandlercli.h"
#include "protocol.h"

#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <string>

using std::cerr;
using std::cin;
using std::cout;
using std::endl;
using std::exception;
using std::invalid_argument;
using std::stoi;
using std::string;

/*
 * Prints a list of possible commands
 */
void printHelpText() {
  cout << "Below is a list of available commands:" << endl;
  cout << "1: list newsgroups" << endl;
  cout << "2: create newsgroup" << endl;
  cout << "3: delete newsgroup" << endl;
  cout << "4: list articles" << endl;
  cout << "5: create article" << endl;
  cout << "6: delete article" << endl;
  cout << "7: get article" << endl;
}

/*
 * Send an integer to the server as four bytes.
 */
void writeNumber(const Connection &conn, int value) {
  conn.write((value >> 24) & 0xFF);
  conn.write((value >> 16) & 0xFF);
  conn.write((value >> 8) & 0xFF);
  conn.write(value & 0xFF);
}

/*
 * Read a string from the server.
 */
string readString(const Connection &conn) {
  string s;
  char ch;
  while ((ch = conn.read()) != '$') {
    s += ch;
  }
  return s;
}

/* Creates a client for the given args, if possible.
 * Otherwise exits with error code.
 */
Connection init(int argc, char *argv[]) {
  if (argc != 3) {
    cerr << "Usage: myclient host-name port-number" << endl;
    exit(1);
  }

  int port = -1;
  try {
    port = stoi(argv[2]);
  } catch (exception &e) {
    cerr << "Wrong port number. " << e.what() << endl;
    exit(2);
  }

  Connection conn(argv[1], port);
  if (!conn.isConnected()) {
    cerr << "Connection attempt failed" << endl;
    exit(3);
  }

  return conn;
}
/*
 * Handles the response from the server given the command ANS_LIST_NG
 * and outputs the result using the parameter conn.
 */
void list_newsgroups(const Connection &conn) {
  int amount = recieve_int(conn);
  if (amount == 0) {
    cout << "No newsgroups exist." << endl;
  } else {
    cout << amount << " newsgroups were found." << endl;
    for (int i = 0; i < amount; i++) {
      int id = recieve_int(conn);
      string title = recieve_string(conn);
      cout << id << " " << title << endl;
    }
  }
  if (recieve_commandbyte(conn) != Protocol::ANS_END) {
    throw ConnectionClosedException();
  }
}
/*
 * Handles the response from the server given the command ANS_CREATE_NG
 * and outputs the result using the parameter conn.
 */
void create_newsgroup(const Connection &conn) {
  Protocol resp = recieve_commandbyte(conn);
  if (resp == Protocol::ANS_ACK) {
    cout << "newsgroup was created" << endl;
  } else if (resp == Protocol::ANS_NAK) {
    Protocol code = recieve_commandbyte(conn);
    if (code == Protocol::ERR_NG_ALREADY_EXISTS) {
      cout << "newsgroup was not created because the newsgroup already exists"
           << endl;
    } else {
      cout << "newsgroup was not created due to unknown error" << endl;
    }
  } else {
    throw ConnectionClosedException();
  }
  if (recieve_commandbyte(conn) != Protocol::ANS_END) {
    throw ConnectionClosedException();
  }
}
/*
 * Handles the response from the server given the command ANS_DELETE_NG
 * and outputs the result using the parameter conn.
 */
void delete_newsgroup(const Connection &conn) {
  Protocol resp = recieve_commandbyte(conn);
  if (resp == Protocol::ANS_ACK) {
    cout << "newsgroup was deleted" << endl;
  } else if (resp == Protocol::ANS_NAK) {
    Protocol code = recieve_commandbyte(conn);
    if (code == Protocol::ERR_NG_DOES_NOT_EXIST) {
      cout << "newsgroup was not deleted because the newsgroup does not exist"
           << endl;
    } else {
      cout << "newsgroup was not deleted due to unknown error" << endl;
    }
  } else {
    throw ConnectionClosedException();
  }
  if (recieve_commandbyte(conn) != Protocol::ANS_END) {
    throw ConnectionClosedException();
  }
}
/*
 * Handles the response from the server given the command ANS_LIST_ART
 * and outputs the result using the parameter conn.
 */
void list_articles(const Connection &conn) {
  Protocol res = recieve_commandbyte(conn);
  if (res == Protocol::ANS_ACK) {
    int amount = recieve_int(conn);
    if (amount == 0) {
      cout << "No articles for this newsgroup exist." << endl;
    } else {
      cout << amount << " articles were found." << endl;
      for (int i = 0; i < amount; i++) {
        int id = recieve_int(conn);
        string title = recieve_string(conn);
        cout << id << " " << title << endl;
      }
    }
  } else if (res == Protocol::ANS_NAK) {
    Protocol code = recieve_commandbyte(conn);
    if (code == Protocol::ERR_NG_DOES_NOT_EXIST) {
      cout << "newsgroup does not exist" << endl;
    } else {
      cout << "no articles were found due to unknown error" << endl;
    }
  }
  if (recieve_commandbyte(conn) != Protocol::ANS_END) {
    throw ConnectionClosedException();
  }
}
/*
 * Handles the response from the server given the command ANS_CREATE_ART
 * and outputs the result using the parameter conn.
 */
void create_article(const Connection &conn) {
  Protocol resp = recieve_commandbyte(conn);
  if (resp == Protocol::ANS_ACK) {
    cout << "article was created" << endl;
  } else if (resp == Protocol::ANS_NAK) {
    Protocol code = recieve_commandbyte(conn);
    if (code == Protocol::ERR_NG_DOES_NOT_EXIST) {
      cout << "article was not created because the newsgroup does not exist"
           << endl;
    } else {
      cout << "article was not created due to unknown error" << endl;
    }
  } else {
    throw ConnectionClosedException();
  }
  if (recieve_commandbyte(conn) != Protocol::ANS_END) {
    throw ConnectionClosedException();
  }
}
/*
 * Handles the response from the server given the command ANS_DELETE_ART
 * and outputs the result using the parameter conn.
 */
void delete_article(const Connection &conn) {
  Protocol resp = recieve_commandbyte(conn);
  if (resp == Protocol::ANS_ACK) {
    cout << "article was deleted" << endl;
  } else if (resp == Protocol::ANS_NAK) {
    Protocol code = recieve_commandbyte(conn);
    if (code == Protocol::ERR_NG_DOES_NOT_EXIST) {
      cout << "article was not deleted because the newsgroup does not exist"
           << endl;
    } else if (code == Protocol::ERR_ART_DOES_NOT_EXIST) {
      cout << "article was not deleted because it does not exist" << endl;
    } else {
      cout << "article was not deleted due to unknown error" << endl;
    }
  } else {
    throw ConnectionClosedException();
  }
  if (recieve_commandbyte(conn) != Protocol::ANS_END) {
    throw ConnectionClosedException();
  }
}
/*
 * Handles the response from the server given the command ANS_GET_ART
 * and outputs the result using the parameter conn.
 */
void get_article(const Connection &conn) {
  Protocol resp = recieve_commandbyte(conn);
  if (resp == Protocol::ANS_ACK) {
    string title = recieve_string(conn);
    string author = recieve_string(conn);
    string text = recieve_string(conn);
    cout << "Title: " << title << endl;
    cout << "Author: " << author << endl;
    cout << text << endl;
  } else if (resp == Protocol::ANS_NAK) {
    Protocol code = recieve_commandbyte(conn);
    if (code == Protocol::ERR_NG_DOES_NOT_EXIST) {
      cout << "article was not retrieved because the newsgroup does not exist"
           << endl;
    } else if (code == Protocol::ERR_ART_DOES_NOT_EXIST) {
      cout << "article was not retrieved because it does not exist" << endl;
    } else {
      cout << "article was not retrieved due to unknown error" << endl;
    }
  } else {
    throw ConnectionClosedException();
  }
  if (recieve_commandbyte(conn) != Protocol::ANS_END) {
    throw ConnectionClosedException();
  }
}
/*
 * Handles the response from the server using the parameter conn.
 * Passes the parameter conn to the appropriate function depending
 * on the command the user has requested.
 */
void handleResponse(const Connection &conn) {
  Protocol nbr = recieve_commandbyte(conn);
  switch (nbr) {
  case Protocol::ANS_LIST_NG:
    list_newsgroups(conn);
    break;

  case Protocol::ANS_CREATE_NG:
    create_newsgroup(conn);
    break;

  case Protocol::ANS_DELETE_NG:
    delete_newsgroup(conn);
    break;

  case Protocol::ANS_LIST_ART:
    list_articles(conn);
    break;

  case Protocol::ANS_CREATE_ART:
    create_article(conn);
    break;

  case Protocol::ANS_DELETE_ART:
    delete_article(conn);
    break;

  case Protocol::ANS_GET_ART:
    get_article(conn);
    break;

  default:
    cout << "Recieved unknown response from server. Exiting..." << endl;
    throw ConnectionClosedException();
  }
}

/*
 * Handles user input for list newsgroups command and
 * sends the request to the server.
 */
void list_newsgroups_input(const Connection &conn) {
  send_commandbyte(conn, Protocol::COM_LIST_NG);
  send_commandbyte(conn, Protocol::COM_END);
}

/*
 * Handles user input for create newsgroup command and
 * sends the request to the server.
 */
void create_newsgroup_input(const Connection &conn) {
  cout << "Please enter the name of the newsgroup that you wish to create:"
       << endl;
  string par;
  cin >> par;
  send_commandbyte(conn, Protocol::COM_CREATE_NG);
  send_string_with_parameter(conn, par);
  send_commandbyte(conn, Protocol::COM_END);
}

/*
 * Handles user input for delete newsgroup command and
 * sends the request to the server.
 */
void delete_newsgroup_input(const Connection &conn) {
  cout << "Please enter the id of the newsgroup that you wish to delete:"
       << endl;
  string par;
  int parint = 0;
  while (cin >> par) {
    try {
      parint = stoi(par);
      break;
    } catch (invalid_argument &) {
      cout << "Please enter a number." << endl;
    }
  }
  send_commandbyte(conn, Protocol::COM_DELETE_NG);
  send_int_with_parameter(conn, parint);
  send_commandbyte(conn, Protocol::COM_END);
}

/*
 * Handles user input for list articles command and
 * sends the request to the server.
 */
void list_articles_input(const Connection &conn) {
  cout << "Please enter the id of the newsgroup whose articles you would like "
          "to see:"
       << endl;
  string par;
  int parint = 0;
  while (cin >> par) {
    try {
      parint = stoi(par);
      break;
    } catch (invalid_argument &) {
      cout << "Please enter a number." << endl;
    }
  }
  send_commandbyte(conn, Protocol::COM_LIST_ART);
  send_int_with_parameter(conn, parint);
  send_commandbyte(conn, Protocol::COM_END);
}

/*
 * Handles user input for create article command and
 * sends the request to the server.
 */
void create_article_input(const Connection &conn) {
  cout << "Please enter the id of the newsgroup in which you would like to "
          "create an article:"
       << endl;
  string par;
  string par2;
  string par3;
  int parint = 0;
  while (cin >> par) {
    try {
      parint = stoi(par);
      break;
    } catch (invalid_argument &) {
      cout << "Please enter a number." << endl;
    }
  }
  cout << "Enter the title:" << endl;
  cin >> par;
  cout << "Enter the author:" << endl;
  cin >> par2;
  cout << "Enter the text:" << endl;
  cin >> par3;
  send_commandbyte(conn, Protocol::COM_CREATE_ART);
  send_int_with_parameter(conn, parint);
  send_string_with_parameter(conn, par);
  send_string_with_parameter(conn, par2);
  send_string_with_parameter(conn, par3);
  send_commandbyte(conn, Protocol::COM_END);
}

/*
 * Handles user input for delete article command and
 * sends the request to the server.
 */
void delete_article_input(const Connection &conn) {
  cout << "Please enter the id of the newsgroup in which you would like to "
          "delete an article:"
       << endl;
  string par;
  int parint = 0;
  int parint2 = 0;
  while (cin >> par) {
    try {
      parint = stoi(par);
      break;
    } catch (invalid_argument &) {
      cout << "Please enter a number." << endl;
    }
  }
  cout << "Please enter the id of the article you would like to delete:"
       << endl;
  while (cin >> par) {
    try {
      parint2 = stoi(par);
      break;
    } catch (invalid_argument &) {
      cout << "Please enter a number." << endl;
    }
  }
  send_commandbyte(conn, Protocol::COM_DELETE_ART);
  send_int_with_parameter(conn, parint);
  send_int_with_parameter(conn, parint2);
  send_commandbyte(conn, Protocol::COM_END);
}

/*
 * Handles user input for get article command and
 * sends the request to the server.
 */
void get_article_input(const Connection &conn) {
  cout << "Please enter the id of the newsgroup in which you would like to "
          "retrieve an article:"
       << endl;
  string par;
  int parint = 0;
  int parint2 = 0;
  while (cin >> par) {
    try {
      parint = stoi(par);
      break;
    } catch (invalid_argument &) {
      cout << "Please enter a number." << endl;
    }
  }
  cout << "Please enter the id of the article you would like to retrieve:"
       << endl;
  while (cin >> par) {
    try {
      parint2 = stoi(par);
      break;
    } catch (invalid_argument &) {
      cout << "Please enter a number." << endl;
    }
  }
  send_commandbyte(conn, Protocol::COM_GET_ART);
  send_int_with_parameter(conn, parint);
  send_int_with_parameter(conn, parint2);
  send_commandbyte(conn, Protocol::COM_END);
}

/*
 * Handles the user input from the parameter nbr.
 * The parameter nbr corresponds to a command which the user
 * has selected.
 * Passes the parameter conn to the appropriate function depending
 * on the command the user has requested.
 */
void handleInput(const Connection &conn, int nbr) {
  Protocol cmd = static_cast<Protocol>(nbr);
  switch (cmd) {
  case Protocol::COM_LIST_NG:
    list_newsgroups_input(conn);
    break;

  case Protocol::COM_CREATE_NG:
    create_newsgroup_input(conn);
    break;

  case Protocol::COM_DELETE_NG:
    delete_newsgroup_input(conn);
    break;

  case Protocol::COM_LIST_ART:
    list_articles_input(conn);
    break;

  case Protocol::COM_CREATE_ART:
    create_article_input(conn);
    break;

  case Protocol::COM_DELETE_ART:
    delete_article_input(conn);
    break;

  case Protocol::COM_GET_ART:
    get_article_input(conn);
    break;

  default:
    cout << "Recieved unknown command from input. Exiting..." << endl;
    throw ConnectionClosedException();
  }
}

int app(const Connection &conn) {
  cout << "Welcome to this usenet!" << endl;
  printHelpText();
  string input;
  while (cin >> input) {
    try {
      int inp = stoi(input);
      if (inp < 1 || inp > 7) {
        cout << "Invalid input. Try again." << endl;
        printHelpText();
        continue;
      }
      handleInput(conn, inp);
      handleResponse(conn);
      cout << "Please enter a new command." << endl;
      printHelpText();
    } catch (ConnectionClosedException &) {
      cout << " no reply from server. Exiting." << endl;
      return 1;
    } catch (invalid_argument &) {
      cout << "Invalid input. Try again." << endl;
      printHelpText();
      continue;
    }
  }
  cout << "\nexiting.\n";
  return 0;
}

int main(int argc, char *argv[]) {
  Connection conn = init(argc, argv);
  return app(conn);
}
