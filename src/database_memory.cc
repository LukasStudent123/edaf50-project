
#include <vector>
#include <algorithm>

#include "database_interface.h"
#include "database_memory.h"

DatabaseMemory::DatabaseMemory() {}

const std::vector<Newsgroup> DatabaseMemory::getAllNewsgroups()
{
  std::vector<Newsgroup> newsgroup_vec;
  for(auto it : newsgroups)
  {
    newsgroup_vec.push_back(it.second);
  }
  return newsgroup_vec;
}

bool DatabaseMemory::addNewsgroup(std::string newsgroup_title)
{
  //Check if title exists
  for(auto it : newsgroups)
  {
    if(it.second.getNewsgroupTitle() == newsgroup_title)
    {
      return false;
    }
  }
  Newsgroup new_newsgroup(news_id, newsgroup_title);
  newsgroups.emplace(news_id,new_newsgroup);
  news_id++;
  return true;
}

bool DatabaseMemory::deleteNewsgroup(ID_Number id)
{
  if(newsgroups.count(id) == 0)
  {
    // No newsgroup with this id
    return false;
  }
  newsgroups.erase(id);
  return true;
}

bool DatabaseMemory::addArticle(ID_Number newsgroup_id, std::string article_title, std::string author, std::string content)
{
  if(newsgroups.count(newsgroup_id) == 0)
  {
    //No newsgroup with ID to add
    return false;
  }
  newsgroups.at(newsgroup_id).createArticle(article_title, author, content);
  return true;
}

const std::pair<int,std::vector<Article>> DatabaseMemory::getAllArticles(ID_Number newsgroup_id)
{
  std::vector<Article> art_vec;
  if(newsgroups.count(newsgroup_id) == 0)
  {
    return {-1, art_vec};
  }
  auto articles = newsgroups.at(newsgroup_id).getArticles();
  
  for(auto art_it : articles)
  {
    art_vec.push_back(art_it.second);
  }
  return {0, art_vec}; 
}

int DatabaseMemory::deleteArticle(ID_Number newsgroup_id, ID_Number article_id)
{
  if(newsgroups.count(newsgroup_id) == 0)
  {
    return -1;
  }
	if (newsgroups.at(newsgroup_id).getArticles().count(article_id) == 0)
	{
		//No article with this id to delete
		return 1;
	}
  newsgroups.at(newsgroup_id).deleteArticle(article_id);
  return 0;
}

std::pair<int,Article> DatabaseMemory::getArticle(ID_Number newsgroup_id, ID_Number article_id)
{
  
	if(newsgroups.count(newsgroup_id) == 0)
  {
    Article f(-1,"","","");
    return {-1,f};
  }
  Newsgroup& ng = newsgroups.at(newsgroup_id);
  std::pair<int, Article> art_pair = ng.getArticle(article_id);

  if(art_pair.first != 0)
  {
    Article f(-1,"","","");
    return {1,f};
  }

  return art_pair;
}
