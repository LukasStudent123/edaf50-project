
.PHONY: all install clean uninstall

all: 
	make -C src

install:
	rm -rf bin
	mkdir bin
	cp src/server_disk bin/server_disk
	cp src/server_memory bin/server_memory
	cp src/myclient bin/myclient

clean:
	make -C src clean

distclean:
	make -C src distclean

uninstall: distclean
	rm -r bin
	rm src/server_disk
	rm src/server_memory
	rm src/myclient
	