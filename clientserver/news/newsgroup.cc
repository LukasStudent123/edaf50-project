#include <iostream>

#include "newsgroup.h"
#include "article.h"



ID_Number Newsgroup::createArticle(std::string title, std::string author, std::string text)
{
	Article art{art_id, title, author, text};
	articles.insert({art_id, art});
	return art_id++;
}
ID_Number Newsgroup::createArticle(std::string title, std::string author, std::string text, ID_Number id)
{
	Article art{id, title, author, text};
	articles.insert({id, art});
	return id;
}


bool Newsgroup::deleteArticle(ID_Number id)
{
	if(articles.count(id) == 0)
	{
		return false;
	}
	articles.erase(id);
	return true;
}

std::pair<int, Article> Newsgroup::getArticle(ID_Number id)
{
	if(articles.count(id)==0)
	{
		return {-1, Article(0,"","","")};
	}
	return {0, articles.at(id)};
}

