
#ifndef DATABASE_DISK_H
#define DATABASE_DISK_H

#include "database_interface.h"
#include "newsgroup.h"
#include "article.h"
#include <vector>
#include <map>

using ID_Number = std::size_t;

class DatabaseDisk : public DatabaseInterface {
public:
	DatabaseDisk();

	const std::vector<Newsgroup> getAllNewsgroups();
	bool addNewsgroup(std::string newsgroup_title);
	bool deleteNewsgroup(ID_Number id);

	const std::pair<int,std::vector<Article>> getAllArticles(ID_Number newsgroup_id); 
	bool addArticle(ID_Number newsgroup_id, std::string article_title, std::string author, std::string content);
	int deleteArticle(ID_Number newsgroup_id, ID_Number article_id);
	std::pair<int,Article> getArticle(ID_Number newsgroup_id, ID_Number article_id);

private: 
	std::map<ID_Number, Newsgroup> newsgroups;
	ID_Number news_id{1}; //+1 each time a newsgroup is added.

  void syncMap();
};

#endif
