#ifndef MESSAGE_HANDLER_H
#define MESSAGE_HANDLER_H

#include <memory>
#include <string>

#include "connection.h"
#include "protocol.h"

class MessageHandler
{
public:
  MessageHandler(std::shared_ptr<Connection>& c) : conn(c) {}; 

  void send_commandbyte(Protocol code);
  void send_int(int value);
  void send_int_with_parameter(int value);
  void send_string_with_parameter(const std::string& s);

  Protocol recieve_commandbyte();
  int recieve_int();
  std::string recieve_string();

  void writeString(const std::string& s);
  int readNumber();
  
private:
  std::shared_ptr<Connection> conn;
  void send_byte(const char code) {conn->write(code);};
  char read_byte(){return conn->read();};
};

#endif
