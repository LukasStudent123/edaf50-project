#include "database_disk.h"
#include <vector>
#include <string>
#include <iostream>
#include <linux/limits.h> //PATH_MAX variable
#include <fstream>

DatabaseDisk::DatabaseDisk() 
{
	syncMap();
}

void DatabaseDisk::syncMap() 
{
	//Syncs newsgroups
	auto *fp = popen("ls", "r");

	char ngArr[PATH_MAX];

	std::map<ID_Number, Newsgroup> newsgroup_map;
	while(fgets(ngArr, PATH_MAX, fp) != nullptr) {
		std::string newsgroup = ngArr;
		std::size_t index = newsgroup.findlastof("_") + 1;
		ID_Number id{std::stoi(newsgroup.substr(index))};
		std::string title{newsgroup.substr(0, index)};
		Newsgroup new_newsgroup(id, title);
		newsgroup_map.emplace(id, new_newsgroup);
	}
	pclose(fp);
	newsgroups = newsgroup_map;
	news_id = newsgroups.rbegin()->first + 1; //Largest ID currently existing among the newsgroups.

	//Syncs articles
	for (auto itr = newsgroups.begin(); itr != newsgroups.end(); ++itr) 
	{
		Newsgroup news = itr->second;
		std::string command = "cd " + news.getNewsgroupTitle + "_" + news.getNewsgroupId;
		system(command);
		auto *fp = popen("ls", "r");

		char ngArr[PATH_MAX];

		//std::map<ID_Number, Newsgroup> articles_map;
		while(fgets(ngArr, PATH_MAX, fp) != nullptr) {
			std::string article = artArr;
			std::size_t idIndex = article.findlastof("_") + 1;
			ID_Number id{std::stoi(article.substr(idIndex))};

			std::string autStr = article.substr(0, idIndex - 1)
			std::size_t autIndex = autStr.findlastof("_") + 1;
			std::string author{autStr.substr(autIndex)};
			std::string title{newsgroup.substr(0, autIndex)};

			std::ifstream file(article);
			
			std::string content((std::istreambuf_iterator<char>(ifs)),(std::istreambuf_iterator<char>()));

			file.close();

			news.createArticle(title, author, content, art_id);
		}
		pclose(fp);
		
		system("cd ..");
	}
	
}

const std::vector<Newsgroup> DatabaseDisk::getAllNewsgroups()
{
  std::vector<Newsgroup> newsgroup_vec;
  for(auto it : newsgroups)
  {
    newsgroup_vec.push_back(it.second);
  }
  return newsgroup_vec;
}
	
bool DatabaseDisk::addNewsgroup(std::string newsgroup_title) 
{
  //Check if title exists
  for(auto it : newsgroups)
  {
    if(it.second.getNewsgroupTitle() == newsgroup_title)
    {
      return false;
    }
  }

  std::string command = "mkdir " + newsgroup_title + "_" + news_id;
  system(command);

  Newsgroup new_newsgroup(news_id, newsgroup_title);
  newsgroups.emplace(news_id,new_newsgroup);
  news_id++;
  return true;
}

bool DatabaseDisk::addArticle(ID_Number newsgroup_id, std::string article_title, std::string author, std::string content)
{
  if(newsgroups.count(newsgroup_id) == 0)
  {
    //No newsgroup with ID to add
    return false;
  }
  Newsgroup news = newsgroups.at(newsgroup_id)
  ID_Number id = news.createArticle(article_title, author, content);
  
  std::string command = "cd " + news.getNewsgroupTitle + "_" + news.getNewsgroupId + " && content >> " + article_title + "_" + author + "_" + id + " && cd ..";
  system(command);

  return true;
}

bool DatabaseDisk::deleteNewsgroup(ID_Number id)
{
  if(newsgroups.count(id) == 0)
  {
    // No newsgroup with this id
    return false;
  }
  newsgroups.erase(id);
  std::string command = "rmdir -r " + newsgroups.at(id).getNewsgroupTitle() + "_" + id;
  system(command);
  return true;
}

const std::vector<Article> DatabaseDisk::getAllArticles(ID_Number newsgroup_id)
{
  std::vector<Article> article_vec;
  for(auto it : newsgroups.at(newsgroup_id).getArticles())
  {
    article_vec.push_back(it.second);
  }
  return article_vec;
}

Article DatabaseDisk::getArticle(ID_Number newsgroup_id, ID_Number article_id)
{
	return newsgroups.at(newsgroup_id).getArticle(article_id);
}
