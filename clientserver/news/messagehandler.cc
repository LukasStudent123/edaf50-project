#include <memory>
#include <string>

#include "messagehandler.h"
#include "connection.h"
#include "connectionclosedexception.h"

/*
* Sends a code according to the protocol
*/
void MessageHandler::send_commandbyte(Protocol code)
{
  send_byte(static_cast<const char>(code));
}

/*
* Sends an int (4 bytes) according to the protocol
*/
void MessageHandler::send_int(int value)
{
  send_byte((value >> 24) & 0xFF);
  send_byte((value >> 16) & 0xFF);
  send_byte((value >> 8) & 0xFF);
  send_byte((value >> 0) & 0xFF);
}

/*
* Sends an int parameter according to the protocol.
* It is automatically preceeded by a PAR_NUM code
*/
void MessageHandler::send_int_with_parameter(int value)
{
  send_commandbyte(Protocol::PAR_NUM);
  send_int(value);
}

/*
* Sends a string parameter according to the protocol.
* It is automatically preceeded by a PAR_STRING code and number of chars
*/
void MessageHandler::send_string_with_parameter(const std::string& s)
{
  int str_len = s.length();
  send_commandbyte(Protocol::PAR_STRING);
  send_int(str_len);
  for(const char c : s)
  {
    send_byte(c);
  }
}


/*
* Recieves a code according to the protocol
*/
Protocol MessageHandler::recieve_commandbyte()
{
  return static_cast<Protocol>(read_byte());
}

/*
* Recieves an int according to the protocol.
* Throws ConnectionClosedException if a PAR_NUM is not present
*/
int MessageHandler::recieve_int()
{
  if(recieve_commandbyte() != Protocol::PAR_NUM)
  {
    throw ConnectionClosedException();
  }
  return readNumber();
}

/*
* Recieves a string according to the protocol.
* Throws ConnectionClosedException if a PAR_STRING is not present
*/
std::string MessageHandler::recieve_string()
{
  if(recieve_commandbyte() != Protocol::PAR_STRING)
  {
    throw ConnectionClosedException();
  }

  int str_len = readNumber();
  std::string recieve;
  for(int i = 0; i < str_len; i++)
  {
    recieve.push_back(read_byte());
  }

  return recieve;
}

int MessageHandler::readNumber()
{
        unsigned char byte1 = conn->read();
        unsigned char byte2 = conn->read();
        unsigned char byte3 = conn->read();
        unsigned char byte4 = conn->read();
        return (byte1 << 24) | (byte2 << 16) | (byte3 << 8) | byte4;
}

void MessageHandler::writeString(const std::string& s)
{
        for (char c : s) {
                conn->write(c);
        }
        conn->write('$');
}
