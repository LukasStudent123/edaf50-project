#ifndef MESSAGE_HANDLER_CLI_H
#define MESSAGE_HANDLER_CLI_H

#include <memory>
#include <string>

#include "connection.h"
#include "connectionclosedexception.h"

void send_byte(const Connection& conn, const char code) {conn.write(code);}
char read_byte(const Connection& conn){return conn.read();}

int readNumber(const Connection& conn)
{
        unsigned char byte1 = conn.read();
        unsigned char byte2 = conn.read();
        unsigned char byte3 = conn.read();
        unsigned char byte4 = conn.read();
        return (byte1 << 24) | (byte2 << 16) | (byte3 << 8) | byte4;
}

void writeString(const Connection& conn, const std::string& s)
{
        for (char c : s) {
                conn.write(c);
        }
        conn.write('$');
}


/*
* Sends a code according to the protocol
*/
void send_commandbyte(const Connection& conn, Protocol code)
{
  send_byte(conn, static_cast<const char>(code));
}

/*
* Sends an int (4 bytes) according to the protocol
*/
void send_int(const Connection& conn, int value)
{
  send_byte(conn, (value >> 24) & 0xFF);
  send_byte(conn, (value >> 16) & 0xFF);
  send_byte(conn, (value >> 8) & 0xFF);
  send_byte(conn, (value >> 0) & 0xFF);
}

/*
* Sends an int parameter according to the protocol.
* It is automatically preceeded by a PAR_NUM code
*/
void send_int_with_parameter(const Connection& conn, int value)
{
  send_commandbyte(conn, Protocol::PAR_NUM);
  send_int(conn, value);
}

/*
* Sends a string parameter according to the protocol.
* It is automatically preceeded by a PAR_STRING code and number of chars
*/
void send_string_with_parameter(const Connection& conn, const std::string& s)
{
  int str_len = s.length();
  send_commandbyte(conn, Protocol::PAR_STRING);
  send_int(conn, str_len);
  for(const char c : s)
  {
    send_byte(conn, c);
  }
}


/*
* Recieves a code according to the protocol
*/
Protocol recieve_commandbyte(const Connection& conn)
{
  return static_cast<Protocol>(read_byte(conn));
}

/*
* Recieves an int according to the protocol.
* Throws ConnectionClosedException if a PAR_NUM is not present
*/
int recieve_int(const Connection& conn)
{
  if(recieve_commandbyte(conn) != Protocol::PAR_NUM)
  {
    throw ConnectionClosedException();
  }
  return readNumber(conn);
}

/*
* Recieves a string according to the protocol.
* Throws ConnectionClosedException if a PAR_STRING is not present
*/
std::string recieve_string(const Connection& conn)
{
  if(recieve_commandbyte(conn) != Protocol::PAR_STRING)
  {
    throw ConnectionClosedException();
  }

  int str_len = readNumber(conn);
  std::string recieve;
  for(int i = 0; i < str_len; i++)
  {
    recieve.push_back(read_byte(conn));
  }

  return recieve;
}

#endif
