/* myserver.cc: sample server program */
#include "connection.h"
#include "connectionclosedexception.h"
#include "messagehandler.h"
#include "newsgroup.h"
#include "protocol.h"
#include "server.h"
#include "database_interface.h" 
#include "database_memory.h"

#include <cstdlib>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

using std::string;
using std::vector;

vector<Newsgroup> newsgroups;

/*
 * Read an integer from a client.
 */
int readNumber(const std::shared_ptr<Connection> &conn) {
  unsigned char byte1 = conn->read();
  unsigned char byte2 = conn->read();
  unsigned char byte3 = conn->read();
  unsigned char byte4 = conn->read();
  return (byte1 << 24) | (byte2 << 16) | (byte3 << 8) | byte4;
}

Server init(int argc, char *argv[]) {
  if (argc != 2) {
    std::cerr << "Usage: myserver port-number" << std::endl;
    exit(1);
  }

  int port = -1;
  try {
    port = std::stoi(argv[1]);
  } catch (std::exception &e) {
    std::cerr << "Wrong format for port number. " << e.what() << std::endl;
    exit(2);
  }

  Server server(port);
  if (!server.isReady()) {
    std::cerr << "Server initialization error." << std::endl;
    exit(3);
  }
  return server;
}

void list_newsgroups( MessageHandler &msg, DatabaseInterface& db) 
{
  msg.send_commandbyte(Protocol::ANS_LIST_NG);
  
  //Get all newsgroups from database
  vector<Newsgroup> vec_news = db.getAllNewsgroups();
  //Send number of newsgroups
  msg.send_int_with_parameter(vec_news.size());
  if(vec_news.size() != 0)
  {
    //For each newsgroup, send the ID and the title
    for(Newsgroup& n : vec_news)
    {
      msg.send_int_with_parameter(n.getNewsgroupId());
      msg.send_string_with_parameter(n.getNewsgroupTitle());
    }
  }
  msg.send_commandbyte(Protocol::ANS_END);
}

void create_newsgroup(MessageHandler &msg, DatabaseInterface& db) 
{
  // Recieve the newsgroup title from protocol
  string ng_title = msg.recieve_string();

  msg.send_commandbyte(Protocol::ANS_CREATE_NG);

  bool success = db.addNewsgroup(ng_title);
  if(success)
  {
    msg.send_commandbyte(Protocol::ANS_ACK);
  } else {
    msg.send_commandbyte(Protocol::ANS_NAK);
    msg.send_commandbyte(Protocol::ERR_NG_ALREADY_EXISTS);
  }
  msg.send_commandbyte(Protocol::ANS_END);

}

void delete_newsgroup( MessageHandler &msg, DatabaseInterface& db) 
{
  int ng_id = msg.recieve_int();

  msg.send_commandbyte(Protocol::ANS_DELETE_NG);

  bool success = db.deleteNewsgroup(ng_id);
  if(success)
  {
    msg.send_commandbyte(Protocol::ANS_ACK);
  } else {
    msg.send_commandbyte(Protocol::ANS_NAK);
    msg.send_commandbyte(Protocol::ERR_NG_DOES_NOT_EXIST);
  }
  msg.send_commandbyte(Protocol::ANS_END);
}

void list_articles( MessageHandler &msg, DatabaseInterface& db) 
{
  int ng_id = msg.recieve_int();
  
  std::pair<int,std::vector<Article>> art_vec_pair = db.getAllArticles(ng_id);

  msg.send_commandbyte(Protocol::ANS_LIST_ART);

  if(art_vec_pair.first != 0)
  {
    msg.send_commandbyte(Protocol::ANS_NAK);
    msg.send_commandbyte(Protocol::ERR_NG_DOES_NOT_EXIST);
  } else {
    msg.send_commandbyte(Protocol::ANS_ACK);
    msg.send_int_with_parameter(art_vec_pair.second.size());
    if(art_vec_pair.second.size() > 0)
    {
    for(auto it : art_vec_pair.second)
      {
        msg.send_int_with_parameter(it.getId());
        msg.send_string_with_parameter(it.getTitle());
      }
    }
  }
  msg.send_commandbyte(Protocol::ANS_END);
}

void create_article( MessageHandler &msg, DatabaseInterface& db) 
{
  int ng_id = msg.recieve_int();
  string title = msg.recieve_string();
  string author = msg.recieve_string();
  string content = msg.recieve_string();

  msg.send_commandbyte(Protocol::ANS_CREATE_ART);

  bool success = db.addArticle(ng_id, title, author, content);
  if(success)
  {
    msg.send_commandbyte(Protocol::ANS_ACK);
  } else {
    msg.send_commandbyte(Protocol::ANS_NAK);
    msg.send_commandbyte(Protocol::ERR_NG_DOES_NOT_EXIST);
  }

  msg.send_commandbyte(Protocol::ANS_END);
}

void delete_article( MessageHandler &msg, DatabaseInterface& db) 
{
  int ng_id = msg.recieve_int();
  int art_id = msg.recieve_int();

  msg.send_commandbyte(Protocol::ANS_DELETE_ART);

  int res = db.deleteArticle(ng_id, art_id);
  if(res < 0)
  {
    msg.send_commandbyte(Protocol::ANS_NAK);
    msg.send_commandbyte(Protocol::ERR_NG_DOES_NOT_EXIST);
  } else if (res > 0) {
    msg.send_commandbyte(Protocol::ANS_NAK);
    msg.send_commandbyte(Protocol::ERR_ART_DOES_NOT_EXIST);
  } else {
    msg.send_commandbyte(Protocol::ANS_ACK);
  }
  msg.send_commandbyte(Protocol::ANS_END);
}

void get_article( MessageHandler &msg, DatabaseInterface& db) 
{
  int ng_id = msg.recieve_int();
  int art_id = msg.recieve_int();

  msg.send_commandbyte(Protocol::ANS_GET_ART);

  std::pair<int, Article> art_pair = db.getArticle(ng_id, art_id);
  if(art_pair.first < 0)
  {
    msg.send_commandbyte(Protocol::ANS_NAK);
    msg.send_commandbyte(Protocol::ERR_NG_DOES_NOT_EXIST);
  } else if (art_pair.first > 0) {
    msg.send_commandbyte(Protocol::ANS_NAK);
      msg.send_commandbyte(Protocol::ERR_ART_DOES_NOT_EXIST);
  } else {
    msg.send_commandbyte(Protocol::ANS_ACK);
    msg.send_string_with_parameter(art_pair.second.getTitle());
    msg.send_string_with_parameter(art_pair.second.getAuthor());
    msg.send_string_with_parameter(art_pair.second.getText());    
  }

  msg.send_commandbyte(Protocol::ANS_END);
}

void doTask(Protocol &nbr, MessageHandler &msg, DatabaseInterface& db) {
  switch (nbr) {
  case Protocol::COM_LIST_NG:
    list_newsgroups(msg, db);
    break;

  case Protocol::COM_CREATE_NG:
    create_newsgroup(msg, db);
    break;

  case Protocol::COM_DELETE_NG:
    delete_newsgroup(msg, db);
    break;

  case Protocol::COM_LIST_ART:
    list_articles(msg, db);
    break;

  case Protocol::COM_CREATE_ART:
    create_article(msg, db);
    break;

  case Protocol::COM_DELETE_ART:
    delete_article(msg, db);
    break;

  case Protocol::COM_GET_ART:
    get_article(msg, db);
    break;

  default:
    throw ConnectionClosedException();
  }
  if(msg.recieve_commandbyte() != Protocol::COM_END)
  {
    // We expect a COM_END at the end of each message
    std::cerr << "No COM_END\n";
    throw ConnectionClosedException();
  }
}

int main(int argc, char *argv[]) {
  auto server = init(argc, argv);
  DatabaseMemory database;

  while (true) {
    auto conn = server.waitForActivity();
    if (conn != nullptr) {
      MessageHandler msg(conn);
      try {
        Protocol cmd = static_cast<Protocol>(conn->read());
        doTask(cmd, msg, database);
      } catch (ConnectionClosedException &) {
        server.deregisterConnection(conn);
        std::cout << "Client closed connection" << std::endl;
      }
    } else {
      conn = std::make_shared<Connection>();
      server.registerConnection(conn);
      std::cout << "New client connects" << std::endl;
    }
  }
  return 0;
}
