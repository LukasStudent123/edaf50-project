
#ifndef DATABASE_INTERFACE_H
#define DATABASE_INTERFACE_H

#include "newsgroup.h"
#include "article.h"

using ID_Number = std::size_t;

class DatabaseInterface {
public:

	DatabaseInterface() {};

	virtual const std::vector<Newsgroup> getAllNewsgroups() = 0;

	//virtual std::pair<int, Newsgroup&> getNewsgroup(ID_Number id) = 0;

	virtual bool addNewsgroup(std::string newsgroup_title) = 0;
	virtual bool addArticle(ID_Number newsgroup_id, std::string article_title, std::string author, std::string content) = 0;
	virtual bool deleteNewsgroup(ID_Number id) = 0;
	
	virtual const std::pair<int,std::vector<Article>> getAllArticles(ID_Number newsgroup_id) = 0; 
	virtual int deleteArticle(ID_Number newsgroup_id, ID_Number article_id) = 0;
	virtual std::pair<int, Article> getArticle(ID_Number newsgroup_id, ID_Number article_id) = 0;
};

#endif
